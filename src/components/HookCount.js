import { useEffect, useState } from "react";

function HookCount() {
    const [count, setCount] = useState(0);
    const onBtnClick = () => {
        setCount(count + 1);
    }
    useEffect(() => {
        document.title = `You click ${count} times`;
    })
    return (
        <div>
            <p>You click {count} times</p>
            <button onClick={onBtnClick}>Click me</button>
        </div>
    )
}
export default HookCount;