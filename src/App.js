import HookCount from "./components/HookCount";


function App() {
  return (
    <div>
      <HookCount/>
    </div>
  );
}

export default App;
